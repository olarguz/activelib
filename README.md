# Libreria ActiveLib

ActiveLib es un proyecto escrito en Java y usa maven, esta encargado de ocultar algunos aspectos de la creacion de conexiones con el servidor de ActiveMQ.

El IDE utilizado es NetBeans, pero por ser un proyecto que usa como administrador de archivos a maven, puede ser usado en otros IDE, como eclipes, IntelliJ, VS Code.

## Configuracion Maven

El proyecto esta registrado bajo el group com.ejemplo.app y el artefacto ActiveLib, como se muestra en el archivo pom.xml.

```
<groupId>com.ejemplo.app</groupId>
<artifactId>ActiveLib</artifactId>
<version>1.0-SNAPSHOT</version>
```

Para el uso la libreria de ActiveMQ, utilizan las dependencias de maven, como se muestra abajo.

```
<dependencies>
    <dependency>
        <groupId>org.apache.activemq</groupId>
        <artifactId>activemq-all</artifactId>
        <version>5.16.0</version>
    </dependency>
</dependencies>
```
